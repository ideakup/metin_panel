<?php 

namespace IdeaKup\Metin_Panel;

use Blade;
use Illuminate\Support\ServiceProvider;

Class ManagerServiceProvider extends ServiceProvider
{	
	/**
    * Indicates if loading of the provider is deferred.
    *
    * @var bool
    */

	/**
    * Indicates if loading of the provider is deferred.
    *
    * @var string
    */


	/**
    * Perform post-registration booting of services.
    *
    * @return void
    */
	public function boot()
    {	
        // publish views
        $this->publishes([__DIR__.'/resources/views/pages' => base_path('resources/views/pages')], 'views');
        // publish controllers
        $this->publishes([__DIR__.'/app/Http/Controllers' => app_path('Http/Controllers')], 'controllers');
        // publish model
        $this->publishes([__DIR__.'/app/Models' => app_path('/')], 'models');
        // publish migrations
        $this->publishes([__DIR__.'/database/migrations' => $this->app->databasePath().'/migrations'], 'migrations');
        // publish seeders
        $this->publishes([__DIR__.'/database/seeds' => $this->app->databasePath().'/seeds'], 'seeds');
        
        require __DIR__.'/routes.php';


        /*
        // public config
        $this->publishes([__DIR__.'/config/pagemanager.php' => config_path('backpack/pagemanager.php')]);

        $this->mergeConfigFrom(__DIR__.'/config/pagemanager.php', 'backpack.pagemanager');
        */
    }
}