<?php

namespace App\Http\Controllers;

use App\Metin;
use Illuminate\Http\Request;

class MetinController extends Controller
{
    public function index()
    {	
    	$data = Metin::find(1);
    	return view('pages.metin', array('data' => $data));
    }
}
