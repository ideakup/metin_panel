<?php

use Illuminate\Database\Seeder;

class MetinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('metin')->delete();

        DB::table('metin')->insert([
            'title' => 'Metin Sayfası Titlesi',
            'header' => 'Metin Sayfası Başlığı',
            'content' => 'Metin Sayfası İçeriği',
        ]);
    }
}
