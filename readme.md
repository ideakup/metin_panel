|||||||||||||||||||||||||||||||||||||||||
			COMPOSER INSTALL
|||||||||||||||||||||||||||||||||||||||||

1.
----------------------------
"repositories": [
    {   
        "name": "ideakup/packagemanager",
        "type": "vcs",
        "url":  "git@bitbucket.org:ideakup/packagemanager.git"
    }
],
"require": {
    "ideakup/packagemanager": "dev-master"
}

2.
-----------------------------
composer update

3.
-----------------------------
composer dump-autoload -o

4.
-----------------------------
//add under script to config/app.php, providers
// psr-4-namespace\class-name::class,

IdeaKup\Metin_Panel\ManagerServiceProvider::class,

//run terminal
php artisan vendor:publish --provider="IdeaKup\Metin_Panel\ManagerServiceProvider"

5.
-----------------------------
php artisan vendor:publish

|||||||||||||||||||||||||||||||||||||||||
			GIT PUSH/PULL
|||||||||||||||||||||||||||||||||||||||||

1.
-----------------------------
//go in project folder on local pc
//create first git 
// get bitbucket ssh repositories

git init 
git add . 
git commit -m "First commit" 
git remote add origin git@bitbucket.org:ideakup/packagemanager.git 
git push origin master

2.
-----------------------------
//push commit

git add . 
git commit -m "commit name" 
git push origin master

3.
-----------------------------
//pull commit

git pull origin master

4.
-----------------------------
//give tags

git tag -a v1.0 -m "deafult version"


